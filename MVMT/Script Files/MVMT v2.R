#Connect to Google Analytics
library(rga)
rga.open(instance="ga")
id <- "77511580"
ga$getRefClass()


#Get the data from GA for the last year
	sd <- Sys.Date()-365
	ed <- Sys.Date()
	print(sd)
	print(ed)
	
	d <- ga$getData(id, start.date=sd, end.date=ed, 
	metrics = "ga:revenuePerUser,ga:users,ga:sessionsPerUser, ga:pageLoadTime", 
	dimensions = "ga:PageDepth, ga:visitLength, ga:deviceCategory, ga:userType, ga:searchUsed", 
	start = 1, batch = "TRUE")

summary(d)

library(pastecs)
stat.desc(d)

library(lattice)
#d3 <- transform(d3, PageD = equal.count(na.omit(as.numeric(PageDepth)), number=5, overlap=1/5))
#d3 <- transform(d3, visitL = equal.count(na.omit(as.numeric(visitLength)), number=5, overlap=1/5))
#d3$pageD <- equal.count(is.null(d3$pageDepth_Num), number=5, overlap=1/5)
#d$sesDuration <- equal.count(na.omit(d$sessionDurationBucket), number=5, overlap=1/5)
#d3$revenuePerU <- equal.count(d3$revenuePerUser, number=4, overlap=1/4)
#d$revenuePerUser_Log <- log(ifelse(d$revenuePerUser ==0,0.5, d$revenuePerUser))

d1 <- transform(d, PageDepth_Num = as.numeric(PageDepth))
#d1$PageDepth_Log <- log(ifelse(d1$PageDepth_Num==0, 1, d1$PageDepth_Num))
d1$visitLen_Num = as.numeric(d1$visitLength)
#d1$visitLen_Log <- log(ifelse(d1$visitLen_Num ==0, 1, d1$visitLen_Num))
d1$device <- as.factor(d1$deviceCategory)
#d1$mobile <- ifelse(d1$deviceCategory=="mobile", 1, 0)
#d1$tablet <- ifelse(d1$deviceCategory=="tablet", 1, 0)
d1$returning <- ifelse(d1$userType=="Returning Visitor", 1, 0)
d1$visitLen_Cat <- 	as.factor(ifelse(d1$visitLen_Num<120, 1, 
					ifelse(d1$visitLen_Num<240, 2,
					ifelse(d1$visitLen_Num<360, 3,
					ifelse(d1$visitLen_Num<480, 4,
					ifelse(d1$visitLen_Num<600, 5, 6
					))))))
d1$PageDepth_Cat <- as.factor(ifelse(d1$PageDepth_Num<5, 1,
						ifelse(d1$PageDepth_Num<10, 2,
						ifelse(d1$PageDepth_Num<15, 3, 4))))
d1$Search <- ifelse(d1$searchUsed=="Visits With Site Search", 1, 0)					
d1$revPerU <- d1$revenuePerUser

d1$PageDepth_Num <- NULL
d1$visitLen_Num <- NULL

summary(d1)
d1$searchUsed



#*************START SCATTERPLOT MATRIX CODE*******************
library(gcookbook)
#====================================
panel.cor <- function(x, y, digits=2, prefix="", cex.cor, ...) {
    usr <- par("usr")
    on.exit(par(usr))
    par(usr = c(0, 1, 0, 1))
    r <- abs(cor(x, y, use="complete.obs"))
    txt <- format(c(r, 0.123456789), digits=digits)[1]
    txt <- paste(prefix, txt, sep="")
    if(missing(cex.cor)) cex.cor <- 0.8/strwidth(txt)
    text(0.5, 0.5, txt, cex =  cex.cor * (1 + r) / 2)
}
    
    
#====================================
panel.hist <- function(x, ...) {
    usr <- par("usr")
    on.exit(par(usr))
    par(usr = c(usr[1:2], 0, 1.5) )
    h <- hist(x, plot = FALSE)
    breaks <- h$breaks
    nB <- length(breaks)
    y <- h$counts
    y <- y/max(y)
    rect(breaks[-nB], 0, breaks[-1], y, col="white", ...)
}
    
pairs.spMatrix <- function(xx,...)
		{
			pairs(xx , 
					upper.panel = panel.cor,
                   diag.panel  = panel.hist,
                   lower.panel = panel.smooth, ...) 
		}
 
#====================================

panel.lm <- function (x, y, col = par("col"), bg = NA, pch = par("pch"),
                            cex = 1, col.smooth = "black", ...) {
    points(x, y, pch = pch, col = col, bg = bg, cex = cex)
    abline(stats::lm(y ~ x),  col = col.smooth, ...)
}
#===============SCATTERPLOT MATRICES=====================

#GENERATE STANDARD SCATTERPLOT MATRIX
pairs(d1[,6:10])

#GENERATE SCATTERPLOT MATRIX WITH LOESS SMOOTHED (LOCALLY ESTIMATED) REGRESSION LINE
pairs.spMatrix(d1[,10:14])

#GENERATE SCATTERPLOT MATRIX WITH LINEAR REGRESSION LINE
pairs(d1[, 10:14], upper.panel = panel.cor,
					diag.panel = panel.hist,
					lower.panel = panel.lm)

#===============RUN LINEAR REGRESSION AND PRINT RESULTS=====================
library(aod)
library(ggplot2)
mylog <- lm(revPerU ~ device + returning + visitLen_Cat + PageDepth_Cat + visitLen_Cat:PageDepth_Cat + returning:visitLen_Cat + returning:PageDepth_Cat, data=d1)
summary(mylog)

#===============CREATE 3D SCATTERPLOT=====================
library(rgl)
plot3d(d1$revPerU, d1$PageDepth_Cat, d1$visitLen_Cat, 
	xlab="", ylab="", zlab="", axes=FALSE,
	type="s", size=0.75, lit=FALSE)

#DRAW THE BOX
rgl.bbox(color="grey50", 
		emission="grey50",		
		xlen=0, ylen=0, zlen=0)	#No tick marks
rgl.material(color="black")

#ADD TICK MARKS TO SPECIFIC SIDES
axes3d(edges=c("x--", "y+-", "z--")
		ntick=6,	#6 tick marks on each side
		cex=.75)	#smaller font

#ADD AXIS LABELS
mtext3d("Revenue Per User", edge="x--", line=2)
mtext3d("Page Depth", edge="y+-", line=3)
mtext3d("Time on Site", edge="z--", line=3)

#===============DEFINE UTILITY FUNCTIONS FOR PLOTTING PREDICTION SURFACE=====================
# Given a model, predict zvar from xvar and yvar
# Defaults to range of x and y variables, and a 16x16 grid
predictgrid <- function(model, xvar, yvar, zvar, res = 16, type = NULL) {
# Find the range of the predictor variable. This works for lm and glm
# and some others, but may require customization for others.
xrange <- range(model$model[[xvar]])
yrange <- range(model$model[[yvar]])
newdata <- expand.grid(x = seq(xrange[1], xrange[2], length.out = res),
y = seq(yrange[1], yrange[2], length.out = res))
names(newdata) <- c(xvar, yvar)
newdata[[zvar]] <- predict(model, newdata = newdata, type = type)
newdata
}

# Convert long-style data frame with x, y, and z vars into a list
# with x and y as row/column values, and z as a matrix.
df2mat <- function(p, xvar = NULL, yvar = NULL, zvar = NULL) {
if (is.null(xvar)) xvar <- names(p)[1]
if (is.null(yvar)) yvar <- names(p)[2]
if (is.null(zvar)) zvar <- names(p)[3]
x <- unique(p[[xvar]])
y <- unique(p[[yvar]])
z <- matrix(p[[zvar]], nrow = length(y), ncol = length(x))
m <- list(x, y, z)
names(m) <- c(xvar, yvar, zvar)
m
}

# Function to interleave the elements of two vectors
interleave <- function(v1, v2) as.vector(rbind(v1,v2))


library(rgl)
# Make a copy of the data set
d2 <- d1

# Generate a linear model
mod <- lm(revPerU ~ visitLen_Cat + PageDepth_Cat + visitLen_Cat:PageDepth_Cat
, data=d2)
summary(mod)
# Get predicted values of Revenue/User from visitLen and PageDepth
d2$pred_RPU <- ifelse(predict(mod)<0, 0, predict(mod))

# Get predicted Revenue/User from a grid of wt and disp
revGrid_df <- predictgrid(mod, "PageDepth_Cat", "visitLen_Cat", "revPerU")
revGrid_list <- df2mat(revGrid_df)

# Make the plot with the data points
plot3d(d2$visitLen_Cat, d2$PageDepth_Cat, d2$RevPerU, type="s", size=0.5, lit=FALSE)

# Add the corresponding predicted points (smaller)
#spheres3d(d2$visitLen_Cat, d2$PageDepth_Cat, d2$pred_RPU, alpha=0.4, type="s", size=0.5, lit=FALSE)

# Add line segments showing the error
#segments3d(interleave(d2$visitLen_Cat, d2$visitLen_Cat),
#interleave(d2$PageDepth_Cat, d2$PageDepth_Cat),
#interleave(d2$RevPerU, d2$pred_RPU),
#alpha=0.4, col="red")

# Add the mesh of predicted values
surface3d(revGrid_list$visitLen_Cat, revGrid_list$PageDepth_Cat, revGrid_list$revPerU,
alpha=0.4, front="lines", back="lines")

#===============CREATE 3D SCATTERPLOT=====================
d3 <- d2
d3$Rev <- ifelse(d3$revPerU>31, 31, d3$revPerU)
library(rgl)
plot3d(d3$PageDepth_Cat, d3$visitLen_Cat, d3$Rev, 
	xlab="", ylab="", zlab="", axes=FALSE,
	type="s", size=0.75, lit=FALSE)
	
# Add the corresponding predicted points (smaller)
#spheres3d(d3$PageDepth_Cat, d3$visitLen_Cat, d3$Rev, alpha=0.4, type="s", size=0.5, lit=FALSE)

# Add line segments showing the error
#segments3d(interleave(d3$PageDepth_Cat, d3$PageDepth_Cat),
#interleave(d3$visitLen_Cat, d3$visitLen_Cat),
#interleave(d3$Rev, d3$pred_RPU),
#alpha=0.4, col="red")

#DRAW THE BOX
rgl.bbox(color="grey50", 
		emission="grey50",		
		xlen=0, ylen=0, zlen=0)	#No tick marks
rgl.material(color="black")

#ADD TICK MARKS TO SPECIFIC SIDES
axes3d(edges=c("y++", "z--"),
		ntick=6,	#6 tick marks on each side
		cex=.75)	#smaller font
axes3d(edges=c("x--"),
		ntick=6,	#4 tick marks on this side
		cex=.75)	#smaller font

#ADD AXIS LABELS
mtext3d("Page Depth", edge="x--", line=2)
mtext3d("Time on Site", edge="y++", line=3)
mtext3d("Revenue Per User", edge="z--", line=3)

# Add the mesh of predicted values
surface3d( revGrid_list$visitLen_Cat, revGrid_list$PageDepth_Cat,revGrid_list$revPerU,
alpha=0.4, front="lines", back="lines")
revGrid_list
#====================================
#PULL DIFFERENT FREQUENCY TABLES FOR CATEGORICAL VARIABLES
freq <- table(d1$deviceCategory,d1$userType)
#freq <- table(d1$PageDepth_Cat,d1$visitLen_Cat)
freq
margin.table(freq,1)
margin.table(freq,2)
prop.table(freq)
prop.table(freq,1)
prop.table(freq,2)


#cor(d1[, 10:14], use = "complete.obs") #Correlation Table for Numeric Variables
#symnum(cor(d[, 10:14], use = "complete.obs")) #Easier to read corelation matrix for numeric variables

#=============USEFUL PLOTS=================
library(Hmisc)
bwplot(deviceCategory ~ revenuePerUser, data=d1, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab='Device Category', xlab='Revenue per User')
bwplot(userType ~ revenuePerUser, data=d1, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab='User Type', xlab='Revenue per User')
bwplot(PageDepth_Cat ~ revenuePerUser, data=d1, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab='User Type', xlab='Revenue per User')
bwplot(visitLen_Cat ~ revenuePerUser, data=d1, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab='User Type', xlab='Revenue per User')


densityplot(~revenuePerUser, data=d1)
stripplot(PageDepth_Cat~revenuePerUser, d1)
#histogram(~revenuePerUser | PageDepth_Cat * visitLen_Cat, data = d1)


#=============UTILITIES=================
sapply(d1, mode) #Get data type for variables in the data set
head(d2, n=10) #Pull first 10 observations
head(revGrid_list, n=100)

#==============================
#READ DATA SET FROM TEXT FILE
d <- read.table("/users/ngp08a/desktop/Personal/Consulting/MVMT/Data/MVMT_FabFive.txt") 
#==============================
#WRITE DATA SET TO TEXT FILE
write.table(d, "/users/ngp08a/desktop/Personal/Consulting/MVMT/Data/MVMT_FabFive.txt", sep="\t")

