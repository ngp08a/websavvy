
##########################################################################################################
## RUN THIS SECTION FIRST, APPROVE THE CONNECTION IN YOUR BROWSER, AND PASTE THE CODE INTO THE CONSOLE. ##
## 				YOU WILL ONLY NEED TO GRANT ACCESS THE FIRST TIME YOU RUN THE SCRIPT				    ##
##########################################################################################################

#Connect to Google Analytics
library(rga)
#rga.open(instance="ga") 
rga.open(instance="ga", client.id="429937801429-33ouhd34mgocjvae4a9fusc3mm8k9cuh.apps.googleusercontent.com", client.secret="wtgL25YzGPRWcfxn12oGH5Nw", where="/users/ngp08a/desktop/Personal/Consulting/MVMT/api.txt")

##############################################################################
## ONCE THE ACCESS CODE HAS BEEN ENTERED YOU CAN RUN THE REST OF THE SCRIPT ##
##############################################################################

#=============== GET USER COUNTS & REVENUE FOR ALL VARIABLES ==================

	 #SET ID FOR GA ACCOUNT ACCESS
	 id <- "77511580"
	 
	 #Set date variables to collect data from GA for the last year
		sd <- Sys.Date()-365
		ed <- Sys.Date()
		print(sd)
		print(ed)
		
	#Build matrix of GA fields to grab and their respective number of categories.	
	cells <- c("browser","Categorical", "5", "operatingSystem","Categorical", "5", "PageDepth","Continuous", "6", "visitLength","Continuous", "6", "deviceCategory","Categorical", "0", "userType","Categorical", "0", "searchUsed","Categorical", "0", "country","Categorical", "5", "city","Categorical", "5", "source","Categorical", "10", "nthMonth","Continuous", "0", "nthWeek","Continuous", "0", "dayOfWeekName","Categorical", "0", "userAgeBracket","Categorical", "0", "userGender","Categorical", "0", "interestInMarketCategory","Categorical", "5", "interestAffinityCategory","Categorical", "5", "interestOtherCategory","Categorical", "5", "sessionCount","Continuous", "5", "hasSocialSourceReferral","Categorical", "0")
	 n=20
	 rnames <- c(1:n)
	 cnames <- c("dimension","type","nCat")
	 dim <- matrix(cells, nrow=n, ncol=3, byrow=TRUE, dimnames=list(rnames, cnames))
	 dim
	 
	#Create dataframe to store individual factor correlation statistics
	modResults <- data.frame(factor="R", adjRSquare=0, fValue=0, stringsAsFactors=FALSE)
	cutResults <- data.frame(factor="R", value="V", cut=0, nCuts=0, stringsAsFactors=FALSE)
	#modResults <-NULL
	
	#Loop through variables and grab them one at a time from GA
	r <- 0
	while(r<n) {
		r <- r+1
		c=dim[row=r, column="dimension"]
		type=dim[row=r, column="type"]
		nCat=as.numeric(dim[row=r, column="nCat"])
		dimensions = paste("ga:", c)
		dimensions
		
		#GET DATA
		dLoop <- ga$getData(id, start.date=sd, end.date=ed, metrics = "ga:users, ga:revenuePerUser", dimensions = dimensions, start = 1, batch = "TRUE")
		
		#c="userAgeBracket"
		
		#Define locations to write files
		fName = paste("/users/ngp08a/desktop/Personal/Consulting/MVMT/D/MVMT_",c,".txt", sep="")
		fName_pdf = paste("/users/ngp08a/desktop/Personal/Consulting/MVMT/Graphs/MVMT_",c,".pdf", sep="")
		da <- dLoop
		#da <- read.table(fName) 

		if(type=="Continuous" && as.numeric(nCat)>0){
			
			#Limit the data for defining cut points to segments with at least 500 users
			da1 <- da[da$users>=500, ]
			
			#Define cut points based on number of categories assigned in variable definition
			library(Hmisc)
			cut <- cut2(as.numeric(da1[,eval(c)]), g=nCat, onlycuts=TRUE)
			da$cat <- 0
			cut.ct <- sum(!is.na(cut))
			a <- as.numeric(cut.ct)
				while(a>1) {		
					a <- a-1
					ct1 <- cut[a]
					da$cat <- ifelse(da$cat==0, ifelse(as.numeric(da[,eval(c)])>ct1, a, 0), da$cat)
					
					#Build Reference File for Category Cuts to use later
					cutRow = c(c, NULL, cut[a], cut.ct)
					cutResults <- rbind(cutResults,cutRow)
				}
			
				
								
		#GENERATE GRAPHICS FOR INDIVIDUAL VARIABLES
			pdf(fName_pdf)
				library(Hmisc)
				#Create some basic descriptive plots
				bwplot(cat ~ revenuePerUser, data=da, panel=panel.bpplot, probs=seq(.01,.49,by=.01), 									datadensity=TRUE,ylab=c, xlab='Revenue per User')
				boxplot(revenuePerUser ~ cat, data=da, notch=TRUE, main=c, xlab="Categories", ylab="Revenue Per User")
				#densityplot(~revenuePerUser, data=da)
				tryCatch(scatter.smooth(da[,eval(c)], da$revenuePerUser), error=function(e) NULL )
				
				#Run a linear regression on factored categorical variable
				mod <- lm(revenuePerUser ~ as.factor(cat), data=da, weight=da$users)
				summary(mod)
			
				# Append predicted values of Revenue/User
				da$pred_RPU <- ifelse(predict(mod)<0, 0, predict(mod))
				tryCatch(scatter.smooth(da$pred_RPU, da$revenuePerUser), error=function(e) NULL )
				
				# Add correlations stats to data frame for comparison later on
				f <- summary(mod)$fstatistic
				p <- tryCatch(pf(f[1],f[2],f[3],lower.tail=F), error=function(e) NULL )
				newRow = c(c, summary(mod)$adj.r.squared, p)
				modResults <- rbind(modResults,newRow)
				modResults
			dev.off()
		}
		
		if(type=="Continuous" && as.numeric(nCat)==0){
			
		#GENERATE GRAPHICS FOR INDIVIDUAL VARIABLES
			pdf(fName_pdf)
				library(Hmisc)
				#Run a linear regression on factored categorical variable
				mod <- lm(revenuePerUser ~ da[,eval(c)], data=da, weight=da$users)
				summary(mod)
			
				# Append predicted values of Revenue/User
				da$pred_RPU <- ifelse(predict(mod)<0, 0, predict(mod))
				tryCatch(scatter.smooth(da$pred_RPU, da$revenuePerUser), error=function(e) NULL )
				
				# Add correlations stats to data frame for comparison later on
				f <- summary(mod)$fstatistic
				p <- tryCatch(pf(f[1],f[2],f[3],lower.tail=F), error=function(e) NULL )
				newRow = c(c, summary(mod)$adj.r.squared, p)
				modResults <- rbind(modResults,newRow)
				modResults
			dev.off()
		}
		
		if(type=="Categorical" && as.numeric(nCat)>0){
			
			#Order factor by number of users
			da1 <- da[with(da, order(-users)), ]
			
			
			#Define categories based on number of categories assigned in variable definition
			da$cat   <- 0
			ct <- as.numeric(nCat)
			cut.ct <- as.numeric(nCat)
			ct1=1
				while(ct>0) {	
					da$cat <- ifelse(da$cat==0, ifelse(da[,eval(c)]==da1[,eval(c)][ct1], ct, 0), da$cat)
								
					#Build Reference File for Category Cuts to use later
					cutRow = c(c, da1[,eval(c)][ct1], ct, cut.ct)
					cutResults <- rbind(cutResults,cutRow)
					
					ct <- ct-1
					ct1 <- ct1+1
				}
					
		#GENERATE GRAPHICS FOR INDIVIDUAL VARIABLES
			pdf(fName_pdf)
				library(Hmisc)
				#Create some basic descriptive plots
				bwplot(cat ~ revenuePerUser, data=da, panel=panel.bpplot, probs=seq(.01,.49,by=.01), 									datadensity=TRUE,ylab=c, xlab='Revenue per User')
				boxplot(revenuePerUser ~ cat, data=da, notch=TRUE, main=c, xlab="Categories", ylab="Revenue Per User")
				#densityplot(~revenuePerUser, data=da)
				tryCatch(scatter.smooth(da[,eval(c)], da$revenuePerUser), error=function(e) NULL )
				
				#Run a linear regression on factored categorical variable
				mod <- lm(revenuePerUser ~ as.factor(cat), data=da, weight=da$users)
				summary(mod)
			
				# Append predicted values of Revenue/User
				da$pred_RPU <- ifelse(predict(mod)<0, 0, predict(mod))
				tryCatch(scatter.smooth(da$pred_RPU, da$revenuePerUser), error=function(e) NULL )
				
				# Add correlations stats to data frame for comparison later on
				f <- summary(mod)$fstatistic
				p <- tryCatch(pf(f[1],f[2],f[3],lower.tail=F), error=function(e) NULL )
				newRow = c(c, summary(mod)$adj.r.squared, p)
				modResults <- rbind(modResults,newRow)
				modResults
			dev.off()
		}
		
		if(type=="Categorical" && as.numeric(nCat)==0){
			
			da$cat <- da[,eval(c)]
			da$revPerU <- ifelse(da$revenuePerUser<3, 1, ifelse(da$revenuePerUser<5, 2, 3))
			da$catNum <- as.numeric(rownames(da))
			cut.ct <- sum(!is.na(da[,1]))
			
			#Build Reference File for Category Cuts to use later
			cutRow = c(c, da$cat, da$catNum, cut.ct)
			cutResults <- rbind(cutResults,cutRow)		
			
			
		#GENERATE GRAPHICS FOR INDIVIDUAL VARIABLES
			pdf(fName_pdf)
				library(Hmisc)
				#Create some basic descriptive plots
				tryCatch(bwplot(catNum ~ revenuePerUser, data=da, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab=c, xlab='Revenue per User'), error=function(e) NULL )
				tryCatch(boxplot(revenuePerUser ~ catNum, data=da, notch=TRUE, main=c, xlab="Categories", ylab="Revenue Per User"), error=function(e) NULL )
				#densityplot(~revenuePerUser, data=da)
				tryCatch(scatter.smooth(da$catNum, da$revenuePerUser), error=function(e) NULL )
				
				#Run a linear regression on factored categorical variable
				mod <- lm(revenuePerUser ~ catNum, data=da, weight=da$users)
				summary(mod)
			
				# Append predicted values of Revenue/User
				da$pred_RPU <- ifelse(predict(mod)<0, 0, predict(mod))
				tryCatch(scatter.smooth(da$pred_RPU, da$revenuePerUser), error=function(e) NULL )
				
				# Add correlations stats to data frame for comparison later on
				f <- summary(mod)$fstatistic
				p <- tryCatch(pf(f[1],f[2],f[3],lower.tail=F), error=function(e) NULL )
				newRow = c(c, summary(mod)$adj.r.squared, p)
				modResults <- rbind(modResults,newRow)
				modResults
			dev.off()
		}
		
		da
		#WRITE THE DATA BACK TO TEXT FILES WITH CATEGORIES APPENDED
		fName_ModSum = paste("/users/ngp08a/desktop/Personal/Consulting/MVMT/DATA/MVMT_IndividualFactorCorrelations.txt", sep="")
		write.table(da, fName, sep="\t")
		write.table(modResults, fName_ModSum, sep="\t")
		
		fName_CutRes = paste("/users/ngp08a/desktop/Personal/Consulting/MVMT/DATA/MVMT_CategoryCuts.txt", sep="")
			write.table(cutResults, fName_CutRes, sep="\t")

	}

c
modResults



# #=============== ANALYZE VARIABLES AND PRINT INDIVIDUAL PLOTS ==================

# #Load data from file, define categories, and plot both continuous and categorical variables

	
	# r <- 0
	# while(r<n) {
		
		
	# }
		

	
	
	
# #FINALLY: GRAB THE COMPILED DATA SET FROM GA WITH 7 MOST PREDICTIVE VARIABLES	
		
# #Grab the data: You will need to update the id value to represent the GA account you are trying to access
# id <- "77511580"
# ga$getRefClass()

# #Get the data from GA for the last year
	# sd <- Sys.Date()-365
	# ed <- Sys.Date()
	# print(sd)
	# print(ed)
	
	# d <- ga$getData(id, start.date=sd, end.date=ed, 
	# metrics = "ga:revenuePerUser,ga:users,ga:sessionsPerUser, ga:pageLoadTime", 
	# dimensions = "ga:PageDepth, ga:visitLength, ga:deviceCategory, ga:userType, ga:searchUsed, ga:browser", 
	# start = 1, batch = "TRUE")


# summary(d)

# library(pastecs)
# stat.desc(da)

# library(lattice)
# #d3 <- transform(d3, PageD = equal.count(na.omit(as.numeric(PageDepth)), number=5, overlap=1/5))
# #d3 <- transform(d3, visitL = equal.count(na.omit(as.numeric(visitLength)), number=5, overlap=1/5))
# #d3$pageD <- equal.count(is.null(d3$pageDepth_Num), number=5, overlap=1/5)
# #d$sesDuration <- equal.count(na.omit(d$sessionDurationBucket), number=5, overlap=1/5)
# #d3$revenuePerU <- equal.count(d3$revenuePerUser, number=4, overlap=1/4)
# #d$revenuePerUser_Log <- log(ifelse(d$revenuePerUser ==0,0.5, d$revenuePerUser))

# d1 <- transform(d, PageDepth_Num = as.numeric(PageDepth))
# #d1$PageDepth_Log <- log(ifelse(d1$PageDepth_Num==0, 1, d1$PageDepth_Num))
# d1$visitLen_Num = as.numeric(d1$visitLength)
# #d1$visitLen_Log <- log(ifelse(d1$visitLen_Num ==0, 1, d1$visitLen_Num))
# d1$device <- as.factor(d1$deviceCategory)
# #d1$mobile <- ifelse(d1$deviceCategory=="mobile", 1, 0)
# #d1$tablet <- ifelse(d1$deviceCategory=="tablet", 1, 0)
# d1$returning <- ifelse(d1$userType=="Returning Visitor", 1, 0)
# d1$visitLen_Cat <- 	as.factor(ifelse(d1$visitLen_Num<120, 1, 
					# ifelse(d1$visitLen_Num<240, 2,
					# ifelse(d1$visitLen_Num<360, 3,
					# ifelse(d1$visitLen_Num<480, 4,
					# ifelse(d1$visitLen_Num<600, 5, 6
					# ))))))
# d1$PageDepth_Cat <- as.factor(ifelse(d1$PageDepth_Num<5, 1,
						# ifelse(d1$PageDepth_Num<10, 2,
						# ifelse(d1$PageDepth_Num<15, 3,
						# ifelse(d1$PageDepth_Num<20, 4,
						# ifelse(d1$PageDepth_Num<25, 5, 6))))))
# d1$Search <- ifelse(d1$searchUsed=="Visits With Site Search", 1, 0)
# if("operatingSystem" %in% names(d1)) d1$opSys <- as.factor(ifelse(d1$operatingSystem=="iOS", 1,
	# ifelse(d1$operatingSystem=="Windows", 2,
	# ifelse(d1$operatingSystem=="Android", 3,
	# ifelse(d1$operatingSystem=="Macintosh", 4,
	# ifelse(d1$operatingSystem=="Linux", 5, 0
	# ))))))
# if("browser" %in% names(d1)) d1$browser_Cat <- as.factor(ifelse(d1$browser=="Chrome", 1,
	# ifelse(d1$browser =="Safari", 2,
	# ifelse(d1$browser =="Safari (in-app)", 3,
	# ifelse(d1$browser =="Firefox", 4,
	# ifelse(d1$browser =="Internet Explorer", 5, 0
	# ))))))
			
# d1$revPerU <- d1$revenuePerUser


# d1$PageDepth_Num <- NULL
# d1$visitLen_Num <- NULL

# summary(d1)

# #===============RUN LINEAR REGRESSION AND PRINT RESULTS=====================
# library(aod)
# library(ggplot2)
# mylog <- lm(revPerU ~ device + returning + visitLen_Cat + PageDepth_Cat + returning:PageDepth_Cat:visitLen_Cat , data=d1, weights=users)
# summary(mylog)


# #*************START SCATTERPLOT MATRIX CODE*******************
# library(gcookbook)
# #====================================
# panel.cor <- function(x, y, digits=2, prefix="", cex.cor, ...) {
    # usr <- par("usr")
    # on.exit(par(usr))
    # par(usr = c(0, 1, 0, 1))
    # r <- abs(cor(x, y, use="complete.obs"))
    # txt <- format(c(r, 0.123456789), digits=digits)[1]
    # txt <- paste(prefix, txt, sep="")
    # if(missing(cex.cor)) cex.cor <- 0.8/strwidth(txt)
    # text(0.5, 0.5, txt, cex =  cex.cor * (1 + r) / 2)
# }
    
    
# #====================================
# panel.hist <- function(x, ...) {
    # usr <- par("usr")
    # on.exit(par(usr))
    # par(usr = c(usr[1:2], 0, 1.5) )
    # h <- hist(x, plot = FALSE)
    # breaks <- h$breaks
    # nB <- length(breaks)
    # y <- h$counts
    # y <- y/max(y)
    # rect(breaks[-nB], 0, breaks[-1], y, col="white", ...)
# }
    
# pairs.spMatrix <- function(xx,...)
		# {
			# pairs(xx , 
					# upper.panel = panel.cor,
                   # diag.panel  = panel.hist,
                   # lower.panel = panel.smooth, ...) 
		# }
 
# #====================================

# panel.lm <- function (x, y, col = par("col"), bg = NA, pch = par("pch"),
                            # cex = 1, col.smooth = "black", ...) {
    # points(x, y, pch = pch, col = col, bg = bg, cex = cex)
    # abline(stats::lm(y ~ x),  col = col.smooth, ...)
# }
# #===============SCATTERPLOT MATRICES=====================

# #GENERATE STANDARD SCATTERPLOT MATRIX
# #pairs(d1[,6:10])

# #GENERATE SCATTERPLOT MATRIX WITH LOESS SMOOTHED (LOCALLY ESTIMATED) REGRESSION LINE
# pairs.spMatrix(d1[,11:17])

# #GENERATE SCATTERPLOT MATRIX WITH LINEAR REGRESSION LINE
# #pairs(d1[, 10:14], upper.panel = panel.cor,
# #					diag.panel = panel.hist,
# #					lower.panel = panel.lm)



# #===============CREATE 3D SCATTERPLOT=====================
# library(rgl)
# plot3d(d1$revPerU, d1$PageDepth_Cat, d1$visitLen_Cat, 
	# xlab="", ylab="", zlab="", axes=FALSE,
	# type="s", size=0.75, lit=FALSE)

# #DRAW THE BOX
# rgl.bbox(color="grey50", 
		# emission="grey50",		
		# xlen=0, ylen=0, zlen=0)	#No tick marks
# rgl.material(color="black")

# #ADD TICK MARKS TO SPECIFIC SIDES
# axes3d(edges=c("x--", "y+-", "z--")
		# ntick=6,	#6 tick marks on each side
		# cex=.75)	#smaller font

# #ADD AXIS LABELS
# mtext3d("Revenue Per User", edge="x--", line=2)
# mtext3d("Page Depth", edge="y+-", line=3)
# mtext3d("Time on Site", edge="z--", line=3)

# #===============DEFINE UTILITY FUNCTIONS FOR PLOTTING PREDICTION SURFACE=====================
# # Given a model, predict zvar from xvar and yvar
# # Defaults to range of x and y variables, and a 16x16 grid
# predictgrid <- function(model, xvar, yvar, zvar, res = 16, type = NULL) {
# # Find the range of the predictor variable. This works for lm and glm
# # and some others, but may require customization for others.
# xrange <- range(model$model[[xvar]])
# yrange <- range(model$model[[yvar]])
# newdata <- expand.grid(x = seq(xrange[1], xrange[2], length.out = res),
# y = seq(yrange[1], yrange[2], length.out = res))
# names(newdata) <- c(xvar, yvar)
# newdata[[zvar]] <- predict(model, newdata = newdata, type = type)
# newdata
# }

# # Convert long-style data frame with x, y, and z vars into a list
# # with x and y as row/column values, and z as a matrix.
# df2mat <- function(p, xvar = NULL, yvar = NULL, zvar = NULL) {
# if (is.null(xvar)) xvar <- names(p)[1]
# if (is.null(yvar)) yvar <- names(p)[2]
# if (is.null(zvar)) zvar <- names(p)[3]
# x <- unique(p[[xvar]])
# y <- unique(p[[yvar]])
# z <- matrix(p[[zvar]], nrow = length(y), ncol = length(x))
# m <- list(x, y, z)
# names(m) <- c(xvar, yvar, zvar)
# m
# }

# # Function to interleave the elements of two vectors
# interleave <- function(v1, v2) as.vector(rbind(v1,v2))


# library(rgl)
# # Make a copy of the data set
# d2 <- d1
# d2$visitLen_Cat <- as.numeric(d2$visitLen_Cat)
# d2$PageDepth_Cat <- as.numeric(d2$PageDepth_Cat)

# # Generate a linear model
# mod <- lm(revPerU ~ visitLen_Cat + PageDepth_Cat + visitLen_Cat:PageDepth_Cat, data=d2, weight=d2$users)
# summary(mod)
# # Get predicted values of Revenue/User from visitLen and PageDepth
# d2$pred_RPU <- ifelse(predict(mod)<0, 0, predict(mod))

# # Get predicted Revenue/User from a grid of wt and disp
# revGrid_df <- predictgrid(mod, "PageDepth_Cat", "visitLen_Cat", "revPerU")
# revGrid_list <- df2mat(revGrid_df)

# # Make the plot with the data points
# plot3d(d2$visitLen_Cat, d2$PageDepth_Cat, d2$RevPerU, type="s", size=0.5, lit=FALSE)

# # Add the corresponding predicted points (smaller)
# #spheres3d(d2$visitLen_Cat, d2$PageDepth_Cat, d2$pred_RPU, alpha=0.4, type="s", size=0.5, lit=FALSE)

# # Add line segments showing the error
# #segments3d(interleave(d2$visitLen_Cat, d2$visitLen_Cat),
# #interleave(d2$PageDepth_Cat, d2$PageDepth_Cat),
# #interleave(d2$RevPerU, d2$pred_RPU),
# #alpha=0.4, col="red")

# # Add the mesh of predicted values
# surface3d(revGrid_list$visitLen_Cat, revGrid_list$PageDepth_Cat, revGrid_list$revPerU,
# alpha=0.4, front="lines", back="lines")

# #===============CREATE 3D SCATTERPLOT=====================
# d3 <- d2
# d3$Rev <- ifelse(d3$revPerU>31, 31, d3$revPerU)
# library(rgl)
# plot3d(d3$PageDepth_Cat, d3$visitLen_Cat, d3$Rev, 
	# xlab="", ylab="", zlab="", axes=FALSE,
	# type="s", size=0.75, lit=FALSE)
	
# # Add the corresponding predicted points (smaller)
# #spheres3d(d3$PageDepth_Cat, d3$visitLen_Cat, d3$Rev, alpha=0.4, type="s", size=0.5, lit=FALSE)

# # Add line segments showing the error
# #segments3d(interleave(d3$PageDepth_Cat, d3$PageDepth_Cat),
# #interleave(d3$visitLen_Cat, d3$visitLen_Cat),
# #interleave(d3$Rev, d3$pred_RPU),
# #alpha=0.4, col="red")

# #DRAW THE BOX
# rgl.bbox(color="grey50", 
		# emission="grey50",		
		# xlen=0, ylen=0, zlen=0)	#No tick marks
# rgl.material(color="black")

# #ADD TICK MARKS TO SPECIFIC SIDES
# axes3d(edges=c("y+-", "z--"),
		# ntick=6,	#6 tick marks on each side
		# cex=.75)	#smaller font
# axes3d(edges=c("x--"),
		# ntick=6,	#4 tick marks on this side
		# cex=.75)	#smaller font

# #ADD AXIS LABELS
# mtext3d("Page Depth", edge="x--", line=2)
# mtext3d("Time on Site", edge="y+-", line=3)
# mtext3d("Revenue Per User", edge="z--", line=3)

# # Add the mesh of predicted values
# surface3d( revGrid_list$visitLen_Cat, revGrid_list$PageDepth_Cat,revGrid_list$revPerU,
# alpha=0.4, front="lines", back="lines")
# revGrid_list
# #====================================
# #PULL DIFFERENT FREQUENCY TABLES FOR CATEGORICAL VARIABLES
# freq <- table(d1$deviceCategory,d1$userType)
# #freq <- table(d1$PageDepth_Cat,d1$visitLen_Cat)
# freq
# margin.table(freq,1)
# margin.table(freq,2)
# prop.table(freq)
# prop.table(freq,1)
# prop.table(freq,2)


# #cor(d1[, 10:14], use = "complete.obs") #Correlation Table for Numeric Variables
# #symnum(cor(d[, 10:14], use = "complete.obs")) #Easier to read corelation matrix for numeric variables

# #=============USEFUL PLOTS=================
# library(Hmisc)
# bwplot(deviceCategory ~ revenuePerUser, data=d1, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab='Device Category', xlab='Revenue per User')
# bwplot(userType ~ revenuePerUser, data=d1, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab='User Type', xlab='Revenue per User')
# bwplot(PageDepth_Cat ~ revenuePerUser, data=d1, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab='User Type', xlab='Revenue per User')
# bwplot(visitLen_Cat ~ revenuePerUser, data=d1, panel=panel.bpplot, probs=seq(.01,.49,by=.01), datadensity=TRUE,ylab='User Type', xlab='Revenue per User')


# densityplot(~revenuePerUser, data=d1)
# stripplot(cat~revenuePerUser, da)
# #histogram(~revenuePerUser | PageDepth_Cat * visitLen_Cat, data = d1)


# #=============UTILITIES=================
# sapply(d1, mode) #Get data type for variables in the data set
# head(d2, n=10) #Pull first 10 observations
# head(revGrid_list, n=100)

# #==============================
# #READ DATA SET FROM TEXT FILE
# d <- read.table("/users/ngp08a/desktop/Personal/Consulting/MVMT/Data/MVMT_FabFive.txt") 
# #==============================
# #WRITE DATA SET TO TEXT FILE
# write.table(d, "/users/ngp08a/desktop/Personal/Consulting/MVMT/Data/MVMT_FabFive.txt", sep="\t")

